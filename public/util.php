<?php

# env
define('ENV', 'local');

# config
$extensions = [
    'curl',
    'imagick',
    'imap',
    'json',
    'mbstring',
    'PDO',
    'pdo_mysql',
    'SimpleXML',
    'soap',
    'sqlite3',
    'xdebug',
    'xmlrpc'
];

# wordpress projects
$config_file = 'wp-config.php';
if (file_exists($config_file)) {
    include_once($config_file);
    $hosts = [
        'DB_HOST' => ['name' => DB_HOST, 'port' => 3140]
    ];   
}

# output
echo (ENV == 'local') ? getHeader().startSection() : '';
echo '<h4 style="text-transform: uppercase">extensions</h4>';
foreach ($extensions as $extension) {
    $loaded = print_r(extension_loaded($extension), true);
    if (!empty($loaded))
        echo '<strong>extension '.$extension.'</strong>: '.boolvalToString($loaded).'<br />';
    else
        echo '<strong>extension '.$extension.'</strong>: <span style="font-size:20px; color:red; font-weight:bold; text-transform:uppercase">'.boolvalToString($loaded).'</span><br />';
}

if (!empty($hosts)) {
    echo (ENV == 'local') ? endSection().startSection() : '<br />';
    echo '<h4 style="text-transform: uppercase">hosts</h4>';
    foreach ($hosts as $host) {
        $fp = @fsockopen($host['name'], (!empty($host['port']) ? $host['port'] : '80'),$errCode,$errStr, 1);
        if (!empty($fp))
            echo '<strong>host '.$host['name'].'</strong>: ok.<br />';
        else
            echo '<strong>host '.$host['name'].'</strong> error:  ('.$errCode.') '.$errStr.'<br />';
        fclose($fp);
    }    
}

if (!empty($urls)) {
    echo (ENV == 'local') ? endSection().startSection() : '<br />';
    echo '<h4 style="text-transform: uppercase">urls</h4>';
    foreach ($urls as $url) {
        $headers = get_headers($url);
        if (!empty($headers))
            echo '<strong>url '.$url.'</strong><pre>'.print_r($headers, true).'</pre><br />';
        else
            echo '<strong>url '.$url.'</strong><pre>'.file_get_contents($url).'</pre><br />';
    }
    echo (ENV == 'local') ? endSection().getFooter() : '<br />';
}

# auxiliar functions
function boolvalToString($var) {
    return (boolval($var) ? 'true' : 'false');
}

function getHeader() {
    $html = '<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
        <title>System Requirements</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.deep_purple-pink.min.css">
        <link rel="stylesheet" href="https://getmdl.io/templates/text-only/styles.css">
    </head>
    <body class="mdl-demo mdl-color--grey-100 mdl-color-text--grey-700 mdl-base">
        <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
            <main class="mdl-layout__content">
                <div class="mdl-layout__tab-panel is-active" id="overview">
    ';
    return $html;
}

function getFooter() {
    $html = '
                </div>
            </main>
        </div>
        <script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    </body>
</html>';
    return $html;
}

function startSection() {
    $html = '
        <section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
            <header class="section__play-btn mdl-cell mdl-cell--3-col-desktop mdl-cell--2-col-tablet mdl-cell--4-col-phone mdl-color--teal-100 mdl-color-text--white">
                <i class="material-icons"></i>
            </header>
            <div class="mdl-card mdl-cell mdl-cell--9-col-desktop mdl-cell--6-col-tablet mdl-cell--4-col-phone">
                <div class="mdl-card__supporting-text">
    ';
    return $html;
}

function endSection() {
    $html = '
                </div>
                <div class="mdl-card__actions">
                    <a href="#" class="mdl-button"></a>
                </div>
            </div>
        </section>
    ';
    return $html;
}
